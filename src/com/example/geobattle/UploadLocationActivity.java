package com.example.geobattle;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;


import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import android.support.v4.app.NavUtils;

public class UploadLocationActivity extends Activity {
	private Location m_lastLocation;
	private String m_userName;
	private String m_userEmail;
	private ProgressDialog m_pDialog;
	private JSONParser jParser = new JSONParser();
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_uploadlocation);
		// Show the Up button in the action bar.
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		Bundle bundle = getIntent().getExtras();
		m_lastLocation = bundle.getParcelable(LocationActivity.EXTRA_LOCATION);
		m_userEmail = bundle.getString(LoginActivity.EXTRA_EMAIL);
		Button uploadNewLocation = (Button)findViewById(R.id.uploadNewLocation);
		uploadNewLocation.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				addLocationToDB();
			}
		});
	}
	private void addLocationToDB(){
		Log.i("UploadLocationActivity", "Tried launching task");
		(new InsertLocationTask(this)).execute(new Location[] {m_lastLocation});
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_uploadlocation, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	private class InsertLocationTask extends AsyncTask<Location, Void, String>{
		Context mContext;
		String info;
		public InsertLocationTask(Context context){
			super();
			Log.i("UploadLocationActivity", "Task Constructor");
			mContext = context;
		}
		protected void onPreExecute(){
			super.onPreExecute();
			Log.i("UploadLocationActivity", "Launched Insert Location Task");
			m_pDialog = new ProgressDialog(UploadLocationActivity.this);
			m_pDialog.setMessage("Uploading Location...");
			m_pDialog.setIndeterminate(false);
			m_pDialog.setCancelable(false);
			m_pDialog.show();
		}
		
		protected String doInBackground(Location... args){
			Location loc = args[0];
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("latitude", Double.toString(loc.getLatitude())));
			params.add(new BasicNameValuePair("longitude", Double.toString(loc.getLongitude())));
			params.add(new BasicNameValuePair("op", "1"));
			params.add(new BasicNameValuePair("auth", m_userEmail));
			params.add(new BasicNameValuePair("name", m_userName));
			
			JSONObject json = jParser.makeHttpRequest("http://www.whiledan.com/data.php", "POST", params);
			Log.i("UploadLocationActivity", "BACKGROUND TASK - Insert Location Task");
			
			try{
				int success = json.getInt("success");
				info = json.getString("message");
				if(success==1){
					//Intent i = getIntent();
					
					//setResult(100, i);
					//finish();
				}
				else{
					//failed to save
				}
			} catch(JSONException e){
				e.printStackTrace();
			}
			return "All Done!";
		}
		@Override
		protected void onPostExecute(String result){
			Log.i("UploadLocationActivity", "Dismissed Insert Location Task");
			super.onPostExecute(result);
			m_pDialog.dismiss();
			int duration = Toast.LENGTH_LONG;
			
			Toast toast = Toast.makeText(mContext, info, duration);
			toast.show();
			finish();
		}
		
	}

}
