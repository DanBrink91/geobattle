package com.example.geobattle;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class LocationActivity extends Activity {
	private LocationManager locationManager;
	private TextView mLatLing;
	private TextView mAddress;
	private LocationMessageHandler mHandler;
	
	private boolean mGeocoderAvailable;
	private Location m_lastLocation;
	private Location m_bestLocation;
	
	private String user_email;
	private JSONParser jParser = new JSONParser();
	private static final int UPDATE_ADDRESS = 1;
	private static final int UPDATE_LATLING = 2;
	private static final int NEW_LOCATIONS = 3;

	 private static final int TEN_SECONDS = 10000;
	 private static final int FIVE_METERS = 5;
	 private static final int TEN_METERS = 10;
	 private static final int TWO_MINUTES = 1000 * 60 * 2;

	 public static final String EXTRA_LOCATION = "com.example.geobattle.locationactivity.extra.LOCATION";

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_location);
		
		Bundle bundle = getIntent().getExtras();
		user_email = bundle.getString(LoginActivity.EXTRA_EMAIL);
		
		mLatLing = (TextView)findViewById(R.id.latling);
		mAddress = (TextView)findViewById(R.id.address);
        // The isPresent() helper method is only available on Gingerbread or above.
        mGeocoderAvailable =
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD && Geocoder.isPresent();
		// Show the Up button in the action bar.
		getActionBar().setDisplayHomeAsUpEnabled(true);

		mHandler = new LocationMessageHandler(this);
		Button uploadLocation = (Button)findViewById(R.id.UploadLocation);
		uploadLocation.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Log.i("LocationActivity", "Button Clicked");
				Intent intent = new Intent(LocationActivity.this, UploadLocationActivity.class);
            	intent.putExtra(LoginActivity.EXTRA_EMAIL, user_email);
            	intent.putExtra(LocationActivity.EXTRA_LOCATION, m_bestLocation);
            	startActivity(intent);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_location, menu);
		return true;
	}
	@Override
	public void onStart(){
		super.onStart();
		locationManager = (LocationManager)this.getSystemService(Context.LOCATION_SERVICE);
		
		final boolean gpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		
		if(!gpsEnabled){
			//Should probably do an alert or something first...
			enableLocationSettings();
		}
		Log.i("LocationActivity","Set up Location");
		setupLocation();
		Log.i("LocationActivity","Set up Location Finish");
		//Log.i("LocationActivity","Set up Location");locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, TEN_SECONDS, TEN_METERS, listener);
	}
	//Lets the user change his settings
	private void enableLocationSettings(){
		Log.i("LocationActivity","Location Settings enable intent launching");
		Intent locationSettingsIntent = new Intent(Settings.ACTION_LOCALE_SETTINGS);
		startActivity(locationSettingsIntent);
	}
	
	@Override
	public void onStop(){
		super.onStop();
		locationManager.removeUpdates(listener);
	}
	//Find the best provider
	private void setupLocation(){
		Criteria criteria = new Criteria();
		criteria.setAccuracy(Criteria.ACCURACY_FINE);
		criteria.setCostAllowed(false);
		Log.i("LocationActivity","Set up Location Criteria set");
		String providerName = locationManager.getBestProvider(criteria, true);
		if(providerName != null){
			locationManager.requestLocationUpdates(providerName, TEN_SECONDS, FIVE_METERS, listener);
			Location last = locationManager.getLastKnownLocation(providerName);
			m_lastLocation = last;
			m_bestLocation = last;
			findNearbyLocations(last);
			Message.obtain(mHandler, 
					LocationActivity.UPDATE_LATLING,
					last.getLatitude() + ", "+
					last.getLongitude()).sendToTarget();
		}
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	private void doGeoencoding(Location location){
		(new ReverseGeocodingTask(this)).execute(new Location[] {location});
	}
	private void findNearbyLocations(Location location){
		(new GetLocationTask(this)).execute(new Location[] {location});
	}
	private final LocationListener listener = new LocationListener(){
		@Override
		public void onLocationChanged(Location location){
			Message.obtain(mHandler, 
					LocationActivity.UPDATE_LATLING,
					location.getLatitude() + ", "+
					location.getLongitude()).sendToTarget();
			m_lastLocation = location;
			if(isBetterLocation(location, m_bestLocation)){
				m_bestLocation = location;
				findNearbyLocations(location);
			}
			if(mGeocoderAvailable){
				doGeoencoding(location);
			}
		}
		 @Override
	     public void onProviderDisabled(String provider) {
	     }

	     @Override
	     public void onProviderEnabled(String provider) {
	     }

	     @Override
	     public void onStatusChanged(String provider, int status, Bundle extras) {
	     }

	};
	/** Determines whether one Location reading is better than the current Location fix
	  * @param location  The new Location that you want to evaluate
	  * @param currentBestLocation  The current Location fix, to which you want to compare the new one
	  */
	protected boolean isBetterLocation(Location location, Location currentBestLocation) {
	    if (currentBestLocation == null) {
	        // A new location is always better than no location
	        return true;
	    }

	    // Check whether the new location fix is newer or older
	    long timeDelta = location.getTime() - currentBestLocation.getTime();
	    boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
	    boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
	    boolean isNewer = timeDelta > 0;

	    // If it's been more than two minutes since the current location, use the new location
	    // because the user has likely moved
	    if (isSignificantlyNewer) {
	        return true;
	    // If the new location is more than two minutes older, it must be worse
	    } else if (isSignificantlyOlder) {
	        return false;
	    }

	    // Check whether the new location fix is more or less accurate
	    int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
	    boolean isLessAccurate = accuracyDelta > 0;
	    boolean isMoreAccurate = accuracyDelta < 0;
	    boolean isSignificantlyLessAccurate = accuracyDelta > 200;

	    // Check if the old and new location are from the same provider
	    boolean isFromSameProvider = isSameProvider(location.getProvider(),
	            currentBestLocation.getProvider());

	    // Determine location quality using a combination of timeliness and accuracy
	    if (isMoreAccurate) {
	        return true;
	    } else if (isNewer && !isLessAccurate) {
	        return true;
	    } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
	        return true;
	    }
	    return false;
	}

	/** Checks whether two providers are the same */
	private boolean isSameProvider(String provider1, String provider2) {
	    if (provider1 == null) {
	      return provider2 == null;
	    }
	    return provider1.equals(provider2);
	}
	static class LocationMessageHandler extends Handler{
		WeakReference<LocationActivity> mActivity;
		
		LocationMessageHandler(LocationActivity loc){
			mActivity = new WeakReference<LocationActivity>(loc);
		}
		@Override
		public void handleMessage(Message msg){
			Log.i("LocationActivity","Handling Message!");
			LocationActivity theActivity = mActivity.get();
			switch(msg.what){
			case UPDATE_ADDRESS:
				theActivity.mAddress.setText((String)msg.obj);
				break;
			case UPDATE_LATLING:
				theActivity.mLatLing.setText((String)msg.obj);
				break;
			case NEW_LOCATIONS:
				ArrayList<GeoLocation> g = (ArrayList<GeoLocation>)msg.obj;
				if(g.size() > 0){
					String topLoc = g.get(0).name;
					Toast toast = Toast.makeText(theActivity.getBaseContext(),"New Location Nearby: "+ topLoc, Toast.LENGTH_LONG);
					toast.show();
				}
				break;
			}
		}
	}
	private class ReverseGeocodingTask extends AsyncTask<Location, Void, Void>{
		Context mContext;
		public ReverseGeocodingTask(Context context){
			super();
			mContext = context;
		}
		@Override
		protected Void doInBackground(Location... params) {
			Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
			
			Location loc = params[0];
			List<Address> addresses = null;
			try{
				addresses = geocoder.getFromLocation(loc.getLatitude(), loc.getLongitude(), 1);
			}
			catch(IOException e){
				e.printStackTrace();
				Message.obtain(mHandler, UPDATE_ADDRESS, e.toString()).sendToTarget();
			}
			if(addresses != null && addresses.size() > 0){
				Address address = addresses.get(0);
				String addressText = String.format("%s,  %s, %s", 
						address.getMaxAddressLineIndex() > 0 ? address.getAddressLine(0): "",
						address.getLocality(),
						address.getCountryName());
				Message.obtain(mHandler, UPDATE_ADDRESS, addressText).sendToTarget();
			}
			return null;
		}
		
	}
	//Structure of locations from database
	private class GeoLocation
	{
		public String name;
		private double longitude;
		private double latitude;
		private double lastDistance;
		
		public GeoLocation(String name, double longitude, double latitude, double lastDistance)
		{
			this.name = name;
			this.longitude = longitude;
			this.latitude = latitude;
			this.lastDistance = lastDistance;
		}
	}
	private class GetLocationTask extends AsyncTask<Location, Void, String>{
		Context mContext;
		ArrayList<GeoLocation> locations;
		public GetLocationTask(Context context){
			super();
			Log.i("dLocationActivity", "Task Constructor");
			mContext = context;
			locations = new ArrayList<GeoLocation>();
		}
		protected void onPreExecute(){
			super.onPreExecute();
			Log.i("UploadLocationActivity", "Launched Insert Location Task");
		}
		
		protected String doInBackground(Location... args){
			Location loc = args[0];
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("latitude", Double.toString(loc.getLatitude())));
			params.add(new BasicNameValuePair("longitude", Double.toString(loc.getLongitude())));
			
			JSONObject json = jParser.makeHttpRequest("http://www.whiledan.com/get_close.php", "POST", params);
			Log.i("LocationActivity", "BACKGROUND TASK - Insert Location Task");
			
			try{
				int success = json.getInt("success");
				JSONArray nearLocations = json.getJSONArray("locations");
				if(success==1){
					for(int i =0; i < nearLocations.length(); ++i){
						JSONObject someLocation = nearLocations.getJSONObject(i);
						locations.add(new GeoLocation(someLocation.getString("name"), someLocation.getDouble("longitude"),
								someLocation.getDouble("latitude"), someLocation.getDouble("distance")));
					}
					Message.obtain(mHandler, NEW_LOCATIONS, locations).sendToTarget();
				}
				else{
					//failed to save
				}
			} catch(JSONException e){
				e.printStackTrace();
			}
			return "All Done!";
		}
		@Override
		protected void onPostExecute(String result){
			Log.i("UploadLocationActivity", "Dismissed Find Locations Task");
			super.onPostExecute(result);
		}
		
	}
}
